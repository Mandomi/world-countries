package com.mandomi.worldcountries.data.boundry

import com.mandomi.worldcountries.data.entity.CountryDetailEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.entity.CountryDetail

fun CountryEntity.toCountry() = Country(name, nativeName, capitalCity, alpha2Code, callingCodes, region)

fun CountryDetailEntity.toCountryEntity(): CountryDetail {
    return CountryDetail(
        name,
        nativeName,
        capitalCity,
        region,
        subRegion,
        location,
        borders,
        population,
        area,
        languages,
        currencies,
        domains,
        timeZones,
        alpha2Code,
        callingCodes
    )
}