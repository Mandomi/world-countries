package com.mandomi.worldcountries.data.entity

import com.mandomi.worldcountries.domain.entity.Currency
import com.mandomi.worldcountries.domain.entity.Language
import com.mandomi.worldcountries.domain.entity.Location

data class CountryDetailEntity(
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val region: String?,
    val subRegion: String?,
    val location: Location?,
    val borders: List<String?>?,
    val population: Long?,
    val area: Double?,
    val languages: List<Language?>?,
    val currencies: List<Currency?>?,
    val domains: List<String?>?,
    val timeZones: List<String?>?,
    val alpha2Code: String,
    val callingCodes: List<String>
)