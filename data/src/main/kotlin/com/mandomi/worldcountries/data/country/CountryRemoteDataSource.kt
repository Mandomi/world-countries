package com.mandomi.worldcountries.data.country

import com.mandomi.worldcountries.data.entity.CountryDetailEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.domain.model.Either

interface CountryRemoteDataSource {
    suspend fun countries(): Either<List<CountryEntity>>
    suspend fun countryDetailByName(name: String): Either<CountryDetailEntity>
    suspend fun countryDetailByCode(code: String): Either<CountryDetailEntity>
}