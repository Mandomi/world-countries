package com.mandomi.worldcountries.data.entity

data class CountryEntity(
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val alpha2Code: String,
    val callingCodes: List<String>,
    val region: String?
)