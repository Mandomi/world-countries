package com.mandomi.worldcountries.data.country

import com.mandomi.worldcountries.data.boundry.toCountry
import com.mandomi.worldcountries.data.boundry.toCountryEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.map
import com.mandomi.worldcountries.domain.repository.CountryRepository
import javax.inject.Inject

class CountryRepositoryImpl @Inject constructor(
    private val cacheDataSource: CountryCacheDataSource,
    private val localDataSource: CountryLocalDataSource,
    private val remoteDataSource: CountryRemoteDataSource
) : CountryRepository {

    override suspend fun countries(): Either<List<Country>> {
        return makeCountriesData()
    }

    override suspend fun countryDetailByName(name: String): Either<CountryDetail> {
        return remoteDataSource.countryDetailByName(name).map { it.toCountryEntity() }
    }

    override suspend fun countryDetailByCode(code: String): Either<CountryDetail> {
        return remoteDataSource.countryDetailByCode(code).map { it.toCountryEntity() }
    }

    private suspend fun makeCountriesData(): Either<List<Country>> {
        return if (cacheDataSource.isCached()) {
            Either.Success(cacheDataSource.countries().map { it.toCountry() })
        } else {
            val localData = localDataSource.countries()
            if (localData.isNotEmpty()) {
                saveCountriesOnCache(localData)
                Either.Success(localData.map { it.toCountry() })
            } else {
                val result = remoteDataSource.countries()

                if (result is Either.Success) {
                    saveCountriesOnCache(result.value)
                    saveCountriesOnDB(result.value)
                }

                result.map { countryEntityList ->
                    countryEntityList.map { it.toCountry() }
                }
            }
        }
    }

    private fun saveCountriesOnCache(localData: List<CountryEntity>) {
        cacheDataSource.add(localData)
    }

    private suspend fun saveCountriesOnDB(countryEntityList: List<CountryEntity>) {
        localDataSource.add(countryEntityList)
    }
}