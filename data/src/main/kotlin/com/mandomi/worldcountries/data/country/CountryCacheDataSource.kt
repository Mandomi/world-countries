package com.mandomi.worldcountries.data.country

import com.mandomi.worldcountries.data.entity.CountryEntity

interface CountryCacheDataSource {
    fun countries(): List<CountryEntity>
    fun add(countryEntity: CountryEntity)
    fun add(countries: List<CountryEntity>)
    fun deleteAll()
    fun isCached(): Boolean
}