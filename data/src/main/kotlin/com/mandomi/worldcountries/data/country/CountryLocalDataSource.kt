package com.mandomi.worldcountries.data.country

import com.mandomi.worldcountries.data.entity.CountryEntity

interface CountryLocalDataSource {
    suspend fun countries(): List<CountryEntity>
    suspend fun add(countryEntity: CountryEntity)
    suspend fun add(countries: List<CountryEntity>)
    suspend fun deleteAll()
}