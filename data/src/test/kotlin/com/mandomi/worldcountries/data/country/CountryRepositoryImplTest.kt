package com.mandomi.worldcountries.data.country

import com.mandomi.worldcountries.data.boundry.toCountry
import com.mandomi.worldcountries.data.boundry.toCountryEntity
import com.mandomi.worldcountries.data.factory.CountryFactory.makeCountryDetailEntity
import com.mandomi.worldcountries.data.factory.CountryFactory.makeCountryEntityList
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.Error.ServerError
import com.mandomi.worldcountries.domain.model.getOrNull
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CountryRepositoryImplTest {

    private val cacheDataSource: CountryCacheDataSource = mockk()
    private val localDataSource: CountryLocalDataSource = mockk()
    private val remoteDataSource: CountryRemoteDataSource = mockk()
    private val repository = CountryRepositoryImpl(cacheDataSource, localDataSource, remoteDataSource)

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountries provides data from cache`() = runBlocking {

        val countries = makeCountryEntityList(10)
        every { cacheDataSource.isCached() } returns true
        every { cacheDataSource.countries() } returns countries

        val result = repository.countries()

        verify(exactly = 1) { cacheDataSource.isCached() }
        verify(exactly = 1) { cacheDataSource.countries() }

        verify { localDataSource wasNot called }
        verify { remoteDataSource wasNot called }

        assertEquals(countries.map { it.toCountry() }, result.getOrNull())
    }

    @Test
    fun `getCountries provides data from DataBase`() = runBlocking {

        val countries = makeCountryEntityList(10)
        every { cacheDataSource.isCached() } returns false
        every { cacheDataSource.add(countries) } just Runs
        coEvery { localDataSource.countries() } returns countries

        val result = repository.countries()

        verify(exactly = 1) { cacheDataSource.isCached() }
        verify(exactly = 0) { cacheDataSource.countries() }
        verify(exactly = 1) { cacheDataSource.add(countries) }

        coVerify(exactly = 1) { localDataSource.countries() }
        verify { remoteDataSource wasNot called }

        assertEquals(countries.map { it.toCountry() }, result.getOrNull())
    }

    @Test
    fun `getCountries provides data from remote`() = runBlocking {

        val countries = makeCountryEntityList(10)
        every { cacheDataSource.isCached() } returns false
        every { cacheDataSource.add(countries) } just Runs
        coEvery { localDataSource.countries() } returns emptyList()
        coEvery { localDataSource.add(countries) } just Runs
        coEvery { remoteDataSource.countries() } returns Either.Success(countries)

        val result = repository.countries()

        verify(exactly = 1) { cacheDataSource.isCached() }
        verify(exactly = 0) { cacheDataSource.countries() }
        verify(exactly = 1) { cacheDataSource.add(countries) }

        coVerify(exactly = 1) { localDataSource.countries() }
        coVerify(exactly = 1) { localDataSource.add(countries) }

        coVerify(exactly = 1) { remoteDataSource.countries() }

        assertEquals(countries.map { it.toCountry() }, result.getOrNull())
    }

    @Test
    fun `getCountries from remote fails`() = runBlocking {

        val failure = ServerError("Server Error")
        every { cacheDataSource.isCached() } returns false
        every { cacheDataSource.add(listOf()) } just Runs
        coEvery { localDataSource.countries() } returns emptyList()
        coEvery { localDataSource.add(listOf()) } just Runs
        coEvery { remoteDataSource.countries() } returns Either.Failure(failure)

        val result = repository.countries()

        verify(exactly = 1) { cacheDataSource.isCached() }
        verify(exactly = 0) { cacheDataSource.countries() }
        verify(exactly = 0) { cacheDataSource.add(listOf()) }

        coVerify(exactly = 1) { localDataSource.countries() }
        coVerify(exactly = 0) { localDataSource.add(listOf()) }

        coVerify(exactly = 1) { remoteDataSource.countries() }

        assert(result is Either.Failure)
    }

    @Test
    fun `countryDetailByName success`() = runBlocking {

        val country = makeCountryDetailEntity()
        val name = "Iran"

        coEvery { remoteDataSource.countryDetailByName(any()) } returns Either.Success(country)

        val result = repository.countryDetailByName(name)

        verify { cacheDataSource wasNot called }
        verify { localDataSource wasNot called }

        coVerify(exactly = 1) { remoteDataSource.countryDetailByName(name) }

        assertEquals(country.toCountryEntity(), result.getOrNull())
    }

    @Test
    fun `countryDetailByName fails`() = runBlocking {

        val failure = ServerError("Server Error")
        val name = "Iran"

        coEvery { remoteDataSource.countryDetailByName(any()) } returns Either.Failure(failure)

        val result = repository.countryDetailByName(name)

        verify { cacheDataSource wasNot called }
        verify { localDataSource wasNot called }

        coVerify(exactly = 1) { remoteDataSource.countryDetailByName(name) }

        assert(result is Either.Failure)
    }

    @Test
    fun `countryDetailByCode success`() = runBlocking {

        val country = makeCountryDetailEntity()
        val code = "Ir"

        coEvery { remoteDataSource.countryDetailByCode(any()) } returns Either.Success(country)

        val result = repository.countryDetailByCode(code)

        verify { cacheDataSource wasNot called }
        verify { localDataSource wasNot called }

        coVerify(exactly = 1) { remoteDataSource.countryDetailByCode(code) }

        assertEquals(country.toCountryEntity(), result.getOrNull())
    }

    @Test
    fun `countryDetailByCode fails`() = runBlocking {

        val failure = ServerError("Server Error")
        val code = "Ir"

        coEvery { remoteDataSource.countryDetailByCode(any()) } returns Either.Failure(failure)

        val result = repository.countryDetailByCode(code)

        verify { cacheDataSource wasNot called }
        verify { localDataSource wasNot called }

        coVerify(exactly = 1) { remoteDataSource.countryDetailByCode(code) }

        assert(result is Either.Failure)
    }

}