package com.mandomi.worldcountries.data.factory

import com.mandomi.worldcountries.data.entity.CountryDetailEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.data.factory.DataFactory.randomDouble
import com.mandomi.worldcountries.data.factory.DataFactory.randomLong
import com.mandomi.worldcountries.data.factory.DataFactory.randomString
import com.mandomi.worldcountries.domain.entity.Currency
import com.mandomi.worldcountries.domain.entity.Language
import com.mandomi.worldcountries.domain.entity.Location

internal object CountryFactory {
    fun makeCountryEntityList(count: Int): List<CountryEntity> {
        val countries = mutableListOf<CountryEntity>()
        repeat(count) {
            countries.add(makeCountry())
        }
        return countries
    }

    private fun makeCountry(): CountryEntity {
        return CountryEntity(
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            listOf(randomString(), randomString()),
            randomString()
        )
    }

    fun makeCountryDetailEntity(): CountryDetailEntity {
        return CountryDetailEntity(
            randomString(), randomString(), randomString(), randomString(), randomString(),
            makeLocation(), listOf(randomString(), randomString()), randomLong(), randomDouble(), makeLanguages(),
            makeCurrencies(), listOf(randomString(), randomString()), listOf(randomString(), randomString()),
            randomString(), listOf(randomString(), randomString())
        )
    }

    private fun makeLocation() = Location(randomDouble(), randomDouble())
    private fun makeLanguages() = listOf(Language(randomString(), randomString(), randomString()))
    private fun makeCurrencies() = listOf(Currency(randomString(), randomString(), randomString()))
}