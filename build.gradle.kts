buildscript {
    repositories {
        maven { url = uri("https://plugins.gradle.org/m2/") }
        google()
        jcenter()
    }
    dependencies {
        classpath(com.mandomi.worldcountries.gradle.GradlePlugins.android)
        classpath(com.mandomi.worldcountries.gradle.GradlePlugins.kotlin)
        classpath(com.mandomi.worldcountries.gradle.GradlePlugins.androidNavigation)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

task("clean", Delete::class) {
    delete(rootProject.buildDir)
}