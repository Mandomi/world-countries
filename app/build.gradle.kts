import com.android.build.gradle.internal.dsl.TestOptions
import com.mandomi.worldcountries.gradle.*
import com.mandomi.worldcountries.gradle.GradlePlugins.kotlin
import com.mandomi.worldcountries.gradle.Libs.appcompat
import com.mandomi.worldcountries.gradle.Libs.constraintlayout
import com.mandomi.worldcountries.gradle.Libs.navigationFragment
import com.mandomi.worldcountries.gradle.Libs.navigationUI
import com.mandomi.worldcountries.gradle.Libs.recyclerview
import com.mandomi.worldcountries.gradle.Libs.lifecycleExtensions
import com.mandomi.worldcountries.gradle.Libs.dagger
import com.mandomi.worldcountries.gradle.Libs.daggerAndroid
import com.mandomi.worldcountries.gradle.Libs.daggerAndroidSupport
import com.mandomi.worldcountries.gradle.Libs.daggerCompiler
import com.mandomi.worldcountries.gradle.Libs.daggerAndroidProcessor
import com.mandomi.worldcountries.gradle.Libs.kotlinStdLib
import com.mandomi.worldcountries.gradle.Libs.okhttp
import com.mandomi.worldcountries.gradle.Libs.roomRuntime
import com.mandomi.worldcountries.gradle.TestLibs.androidXTestRunner
import com.mandomi.worldcountries.gradle.TestLibs.junit
import com.mandomi.worldcountries.gradle.TestLibs.espressoCore
import com.mandomi.worldcountries.gradle.TestLibs.mockk
import com.mandomi.worldcountries.gradle.TestLibs.androidXCoreTesting
import com.mandomi.worldcountries.gradle.TestLibs.androidXTestRules
import com.mandomi.worldcountries.gradle.TestLibs.robolectric

plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android-extensions")
    kotlin("kapt")
    id("androidx.navigation.safeargs")
}

android {
    compileSdkVersion(Config.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Config.minSdkVersion)
        targetSdkVersion(Config.targetSdkVersion)
        versionCode = Config.versionCode
        versionName = Config.versionName

        multiDexEnabled = true
        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    dataBinding {
        isEnabled = true
    }
    buildTypes {
        getByName("release") {
            isShrinkResources = true
            isMinifyEnabled = true
            proguardFiles("proguard-rules.pro", getDefaultProguardFile("proguard-android.txt"))
        }
    }
}

dependencies {
    implementations {
        +project(":domain")
        +project(":data")
        +project(":local")
        +project(":remote")
        +appcompat
        +constraintlayout
        +navigationFragment
        +navigationUI
        +recyclerview
        +lifecycleExtensions
        +dagger
        +daggerAndroid
        +daggerAndroidSupport
        +roomRuntime
    }
    kapts {
        +daggerCompiler
        +daggerAndroidProcessor
    }

    testImplementations {
        +junit
        +robolectric
        +mockk
        +androidXCoreTesting
    }
    androidTestImplementations {
        +androidXTestRunner
        +androidXTestRules
        +espressoCore
    }
}