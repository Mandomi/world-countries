package com.mandomi.worldcountries.ui.country.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.interactor.GetCountryDetailByName
import com.mandomi.worldcountries.domain.model.Error
import com.mandomi.worldcountries.domain.model.Error.ServerError
import com.mandomi.worldcountries.factory.CountryFactory.makeCountryDetail
import com.mandomi.worldcountries.ui.model.ResourceState
import io.mockk.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CountryDetailViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val useCase: GetCountryDetailByName = mockk()
    private val viewModel = CountryDetailViewModel(useCase)
    private val countryName = "Iran"

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountryDetailByName executes`() {
        every { useCase.execute(any(), any(), any()) } just Runs
        viewModel.load(countryName)
        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
    }

    @Test
    fun `getCountryDetailByName success and contains data`() {
        val countryDetail = makeCountryDetail()
        val success = slot<(countryDetail: CountryDetail) -> Unit>()

        every {
            useCase.execute(any(), capture(success), any())
        } answers {
            success.invoke(countryDetail)
        }

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.SUCCESS)
        assert(viewModel.getData().value?.data == countryDetail)
    }

    @Test
    fun `getCountryDetailByName success and contains no error`() {
        val countryDetail = makeCountryDetail()
        val success = slot<(countryDetail: CountryDetail) -> Unit>()

        every {
            useCase.execute(any(), capture(success), any())
        } answers {
            success.invoke(countryDetail)
        }

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.SUCCESS)
        assert(viewModel.getData().value?.failure == null)
    }


    @Test
    fun `getCountryDetailByName fails and contains error data`() {
        val error = slot<(throwable: Throwable) -> Unit>()

        every {
            useCase.execute(any(), any(), capture(error))
        } answers {
            error.invoke(ServerError("Server Error"))
        }

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.ERROR)
        assert(viewModel.getData().value?.failure is Error.ServerError)
    }

    @Test
    fun `getCountryDetailByName fails and contain no data`() {
        val error = slot<(throwable: Throwable) -> Unit>()

        every {
            useCase.execute(any(), any(), capture(error))
        } answers {
            error.invoke(ServerError("Server Error"))
        }

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.ERROR)
        assert(viewModel.getData().value?.data == null)
    }

    @Test
    fun `getCountryDetailByName returns loading`() {
        every { useCase.execute(any(), any(), any()) } just Runs

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.LOADING)
    }

    @Test
    fun `getCountryDetailByName contains no data and no error when loading`() {
        every { useCase.execute(any(), any(), any()) } just Runs

        viewModel.load(countryName)

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.LOADING)
        assert(viewModel.getData().value?.data == null)
        assert(viewModel.getData().value?.failure == null)
    }
}