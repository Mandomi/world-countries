package com.mandomi.worldcountries.ui.country.countries

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.interactor.GetCountriesUseCase
import com.mandomi.worldcountries.domain.model.Error.ServerError
import com.mandomi.worldcountries.factory.CountryFactory.makeCountryList
import com.mandomi.worldcountries.ui.country.toCountryItem
import com.mandomi.worldcountries.ui.model.ResourceState
import io.mockk.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class CountriesViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val useCase: GetCountriesUseCase = mockk()
    private val viewModel = CountriesViewModel(useCase)

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountriesUseCase executes`() {
        every { useCase.execute(any(), any(), any()) } just Runs
        viewModel.load()
        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
    }

    @Test
    fun `getCountriesUseCase success and contains data`() {
        val countries = makeCountryList(10)
        val success = slot<(countries: List<Country>) -> Unit>()

        every {
            useCase.execute(any(), capture(success), any())
        } answers {
            success.invoke(countries)
        }

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.SUCCESS)
        assert(viewModel.getData().value?.data == countries.map { it.toCountryItem() })
    }

    @Test
    fun `getCountriesUseCase success and contains no error`() {
        val countries = makeCountryList(10)
        val success = slot<(countries: List<Country>) -> Unit>()

        every {
            useCase.execute(any(), capture(success), any())
        } answers {
            success.invoke(countries)
        }

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.SUCCESS)
        assert(viewModel.getData().value?.failure == null)
    }


    @Test
    fun `getCountriesUseCase fails and contains error data`() {
        val error = slot<(throwable: Throwable) -> Unit>()

        every {
            useCase.execute(any(), any(), capture(error))
        } answers {
            error.invoke(ServerError("Server Error"))
        }

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.ERROR)
        assert(viewModel.getData().value?.failure is ServerError)
    }

    @Test
    fun `getCountriesUseCase fails and contain no data`() {
        val error = slot<(throwable: Throwable) -> Unit>()

        every {
            useCase.execute(any(), any(), capture(error))
        } answers {
            error.invoke(ServerError("Server Error"))
        }

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.ERROR)
        assert(viewModel.getData().value?.data == null)
    }

    @Test
    fun `getCountriesUseCase returns loading`() {
        every { useCase.execute(any(), any(), any()) } just Runs

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.LOADING)
    }

    @Test
    fun `getCountriesUseCase contains no data and no error when loading`() {
        every { useCase.execute(any(), any(), any()) } just Runs

        viewModel.load()

        verify(exactly = 1) { useCase.execute(any(), any(), any()) }
        assert(viewModel.getData().value?.resourceState == ResourceState.LOADING)
        assert(viewModel.getData().value?.data == null)
        assert(viewModel.getData().value?.failure == null)
    }
}