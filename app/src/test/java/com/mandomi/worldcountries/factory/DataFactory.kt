package com.mandomi.worldcountries.factory

import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.ThreadLocalRandom

internal object DataFactory {

    fun randomInt(): Int {
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
    }

    fun randomLong(): Long {
        return randomInt().toLong()
    }

    fun randomDouble(): Double {
        return randomInt().toDouble()
    }

    fun randomString(): String {
        val array = ByteArray(7)
        Random().nextBytes(array)
        return String(array, Charset.forName("UTF-8"))
    }
}