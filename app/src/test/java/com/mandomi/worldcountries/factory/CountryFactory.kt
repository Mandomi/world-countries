package com.mandomi.worldcountries.factory

import com.mandomi.worldcountries.domain.entity.*
import com.mandomi.worldcountries.factory.DataFactory.randomDouble
import com.mandomi.worldcountries.factory.DataFactory.randomLong
import com.mandomi.worldcountries.factory.DataFactory.randomString

internal object CountryFactory {
    fun makeCountryList(count: Int): List<Country> {
        val countries = mutableListOf<Country>()
        repeat(count) {
            countries.add(makeCountry())
        }
        return countries
    }

    fun makeCountry(): Country {
        return Country(
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            listOf(randomString(), randomString()),
            randomString()
        )
    }

    fun makeCountryDetail(): CountryDetail {
        return CountryDetail(
            randomString(), randomString(), randomString(), randomString(), randomString(),
            makeLocation(), listOf(randomString(), randomString()), randomLong(), randomDouble(), makeLanguages(),
            makeCurrencies(), listOf(randomString(), randomString()), listOf(randomString(), randomString()),
            randomString(), listOf(randomString(), randomString())
        )
    }

    private fun makeLocation() = Location(randomDouble(), randomDouble())
    private fun makeLanguages() = listOf(Language(randomString(), randomString(), randomString()))
    private fun makeCurrencies() = listOf(Currency(randomString(), randomString(), randomString()))
}