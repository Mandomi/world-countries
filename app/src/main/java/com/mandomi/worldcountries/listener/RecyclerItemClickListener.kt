package com.mandomi.worldcountries.listener

interface RecyclerItemClickListener<in T> {
    fun onClick(t: T)
}