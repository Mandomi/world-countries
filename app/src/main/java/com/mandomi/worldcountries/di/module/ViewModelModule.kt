package com.mandomi.worldcountries.di.module

import androidx.lifecycle.ViewModelProvider
import com.mandomi.worldcountries.core.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}