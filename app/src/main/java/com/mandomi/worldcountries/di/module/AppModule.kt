package com.mandomi.worldcountries.di.module

import android.content.Context
import com.mandomi.worldcountries.core.WorldCountriesApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: WorldCountriesApplication): Context = application.applicationContext
}