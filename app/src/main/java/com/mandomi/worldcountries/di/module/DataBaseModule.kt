package com.mandomi.worldcountries.di.module

import android.content.Context
import androidx.room.Room
import com.mandomi.worldcountries.local.dao.CountryDao
import com.mandomi.worldcountries.local.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {
    @Singleton
    @Provides
    fun provideCountryDao(appDatabase: AppDatabase): CountryDao = appDatabase.countryDao()

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase = Room
        .databaseBuilder(context.applicationContext, AppDatabase::class.java, "data.db")
        .fallbackToDestructiveMigration()
        .build()
}