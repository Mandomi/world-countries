package com.mandomi.worldcountries.di.component

import com.mandomi.worldcountries.core.WorldCountriesApplication
import com.mandomi.worldcountries.di.module.*
import com.mandomi.worldcountries.ui.country.CountryModule
import com.mandomi.worldcountries.ui.home.HomeModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        AppModule::class,
        NetworkModule::class,
        RetrofitModule::class,
        DataBaseModule::class,
        CountryModule::class,
        HomeModule::class
    ]
)
interface AppComponent : AndroidInjector<WorldCountriesApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<WorldCountriesApplication>()

}