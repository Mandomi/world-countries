package com.mandomi.worldcountries.di.module

import com.mandomi.worldcountries.remote.extension.createRetrofitService
import com.mandomi.worldcountries.remote.retrofit.CountryService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun provideCountryService(okHttpClient: OkHttpClient): CountryService {
        return createRetrofitService(okHttpClient)
    }
}