package com.mandomi.worldcountries.ui.country.countries

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mandomi.worldcountries.R
import com.mandomi.worldcountries.listener.RecyclerItemClickListener

class CountriesAdapter : RecyclerView.Adapter<CountryViewHolder>() {

    var onItemClickListener: RecyclerItemClickListener<CountryItem>? = null

    var items: ArrayList<CountryItem> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val viewHolder =
            CountryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false))
        viewHolder.onItemClickListener = this@CountriesAdapter.onItemClickListener
        return viewHolder
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(items[position])
    }
}