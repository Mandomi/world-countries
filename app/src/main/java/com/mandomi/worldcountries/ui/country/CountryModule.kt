package com.mandomi.worldcountries.ui.country

import androidx.lifecycle.ViewModel
import com.mandomi.worldcountries.data.country.CountryCacheDataSource
import com.mandomi.worldcountries.data.country.CountryLocalDataSource
import com.mandomi.worldcountries.data.country.CountryRemoteDataSource
import com.mandomi.worldcountries.data.country.CountryRepositoryImpl
import com.mandomi.worldcountries.di.annotation.ViewModelKey
import com.mandomi.worldcountries.domain.repository.CountryRepository
import com.mandomi.worldcountries.local.datasource.CountryCacheDataSourceImpl
import com.mandomi.worldcountries.local.datasource.CountryLocalDataSourceImpl
import com.mandomi.worldcountries.remote.datasource.CountryRemoteDataSourceImpl
import com.mandomi.worldcountries.ui.country.countries.CountriesFragment
import com.mandomi.worldcountries.ui.country.countries.CountriesViewModel
import com.mandomi.worldcountries.ui.country.detail.CountryDetailFragment
import com.mandomi.worldcountries.ui.country.detail.CountryDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class CountryModule {

    @ContributesAndroidInjector
    internal abstract fun countriesFragment(): CountriesFragment

    @ContributesAndroidInjector
    internal abstract fun countryDetailFragment(): CountryDetailFragment

    @Binds
    @IntoMap
    @ViewModelKey(CountriesViewModel::class)
    abstract fun bindCountriesViewModel(countriesViewModel: CountriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CountryDetailViewModel::class)
    abstract fun bindCountryDetailViewModel(countryDetailViewModel: CountryDetailViewModel): ViewModel

    @Binds
    abstract fun bindCountryRepository(countryRepository: CountryRepositoryImpl): CountryRepository

    @Binds
    abstract fun bindCountryCacheDataSource(countryCacheDataSource: CountryCacheDataSourceImpl): CountryCacheDataSource

    @Binds
    abstract fun bindCountryLocalDataSource(countryLocalDataSource: CountryLocalDataSourceImpl): CountryLocalDataSource

    @Binds
    abstract fun bindCountryRemoteDataSource(countryRemoteDataSource: CountryRemoteDataSourceImpl): CountryRemoteDataSource
}