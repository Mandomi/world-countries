package com.mandomi.worldcountries.ui.country.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.interactor.GetCountryDetailByName
import com.mandomi.worldcountries.ui.base.BaseViewModel
import com.mandomi.worldcountries.ui.model.Resource
import com.mandomi.worldcountries.ui.model.ResourceState.*
import javax.inject.Inject

class CountryDetailViewModel @Inject constructor(private val useCase: GetCountryDetailByName) : BaseViewModel() {

    private val countryDetail = MutableLiveData<Resource<CountryDetail>>()

    init {
        useCases += useCase
    }

    fun getData(): LiveData<Resource<CountryDetail>> {
        return countryDetail
    }

    fun load(countryName: String) {
        countryDetail.value = Resource(LOADING)
        useCase.execute(GetCountryDetailByName.Params(countryName), ::success, ::error)
    }

    private fun success(countryDetail: CountryDetail) {
        this.countryDetail.value = Resource(SUCCESS, countryDetail)
    }

    private fun error(throwable: Throwable) {
        countryDetail.value = Resource(ERROR, failure = throwable)
    }
}