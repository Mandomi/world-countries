package com.mandomi.worldcountries.ui.country.countries

data class CountryItem(
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val alpha2Code: String,
    val callingCodes: List<String>,
    val region: String?
)