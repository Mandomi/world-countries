package com.mandomi.worldcountries.ui.model

import java.io.Serializable

enum class ResourceState : Serializable {
    SUCCESS,
    LOADING,
    ERROR
}