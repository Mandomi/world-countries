package com.mandomi.worldcountries.ui.country

import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.ui.country.countries.CountryItem

fun Country.toCountryItem() = CountryItem(name, nativeName, capitalCity, alpha2Code, callingCodes, region)