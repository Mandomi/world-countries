package com.mandomi.worldcountries.ui.country.countries

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mandomi.worldcountries.R
import com.mandomi.worldcountries.listener.RecyclerItemClickListener

class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var onItemClickListener: RecyclerItemClickListener<CountryItem>? = null

    private val image: ImageView = itemView.findViewById(R.id.image)
    private val name: TextView = itemView.findViewById(R.id.name)
    private val capital: TextView = itemView.findViewById(R.id.capital)

    fun bind(item: CountryItem) {
        val context = itemView.context

        name.text = String.format(
            context.getString(R.string.name_and_code_and_calling_code),
            item.name,
            item.alpha2Code,
            item.callingCodes.joinToString { "+$it" }
        )

        val id =
            context.resources.getIdentifier("ic_${item.alpha2Code.toLowerCase()}", "drawable", context.packageName)
        image.setImageResource(id)

        if (item.capitalCity.isNullOrEmpty()) {
            capital.visibility = View.GONE
        } else {
            capital.visibility = View.VISIBLE
            capital.text = item.capitalCity
        }

        itemView.setOnClickListener {
            if (onItemClickListener != null) {
                onItemClickListener!!.onClick(item)
            }
        }

    }
}