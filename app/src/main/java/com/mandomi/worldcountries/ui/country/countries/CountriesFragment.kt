package com.mandomi.worldcountries.ui.country.countries

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mandomi.worldcountries.R
import com.mandomi.worldcountries.extension.createViewModel
import com.mandomi.worldcountries.extension.observe
import com.mandomi.worldcountries.listener.RecyclerItemClickListener
import com.mandomi.worldcountries.ui.base.BaseFragment
import com.mandomi.worldcountries.ui.model.Resource
import com.mandomi.worldcountries.ui.model.ResourceState
import kotlinx.android.synthetic.main.fragment_countries.*

class CountriesFragment : BaseFragment(), RecyclerItemClickListener<CountryItem> {
    private lateinit var viewModel: CountriesViewModel

    private val adapter: CountriesAdapter = CountriesAdapter()

    override fun onClick(t: CountryItem) {
        val direction = CountriesFragmentDirections.actionCountriesFragmentToCountryDetailFragment(t.name)
        view?.findNavController()?.navigate(direction)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_countries, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = createViewModel(viewModelFactory) {
            observe(getData(), ::handleResourceState)
        }
        viewModel.load()
    }

    private fun init() {

        with(adapter) {
            onItemClickListener = this@CountriesFragment
        }

        with(recyclerView) {
            setHasFixedSize(false)
            adapter = this@CountriesFragment.adapter
            setPadding(0, 0, 0, 0)
            itemAnimator?.changeDuration = 0
            layoutManager = LinearLayoutManager(requireContext())
        }

        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                // not implemented
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // not implemented
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty()) {
                    viewModel.load(s.toString())
                }
            }

        })
    }

    private fun handleResourceState(resource: Resource<List<CountryItem>>?) {
        resource?.let {
            when (resource.resourceState) {
                ResourceState.LOADING -> handleLoading()
                ResourceState.SUCCESS -> handleSuccess(resource.data!!)
                ResourceState.ERROR -> handleError(resource.failure!!)
            }
        }
    }

    private fun handleLoading() {
        showLoading()
        hideRecyclerView()
        hideErrorView()
        hideEmptyView()
    }

    private fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    private fun hideRecyclerView() {
        recyclerView.visibility = View.GONE
    }

    private fun hideErrorView() {
        errorView.visibility = View.GONE
    }

    private fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    private fun handleSuccess(data: List<CountryItem>) {
        hideLoading()
        hideErrorView()
        if (data.isEmpty()) {
            hideRecyclerView()
            showEmptyView()
        } else {
            hideEmptyView()
            showRecyclerView()
            showData(data)
        }
    }

    private fun hideLoading() {
        loading.visibility = View.GONE
    }

    private fun showRecyclerView() {
        recyclerView.visibility = View.VISIBLE
    }

    private fun showData(items: List<CountryItem>) {
        adapter.items = ArrayList(items)
    }

    private fun handleError(failure: Throwable) {
        hideLoading()
        hideRecyclerView()
        hideEmptyView()
        showErrorView()
        showSnackBar(failure.message)
    }

    private fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    private fun showErrorView() {
        errorView.visibility = View.VISIBLE
    }

    private fun showSnackBar(message: String?) {
        message?.let {
            Snackbar.make(view!!, message, Snackbar.LENGTH_INDEFINITE).apply {
                view.layoutParams = (view.layoutParams as FrameLayout.LayoutParams).apply {
                    setAction(getString(R.string.try_again)) { viewModel.load() }
                }
            }.show()
        }
    }
}
