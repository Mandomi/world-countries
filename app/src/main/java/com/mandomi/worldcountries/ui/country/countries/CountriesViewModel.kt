package com.mandomi.worldcountries.ui.country.countries

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.interactor.GetCountriesUseCase
import com.mandomi.worldcountries.ui.base.BaseViewModel
import com.mandomi.worldcountries.ui.country.toCountryItem
import com.mandomi.worldcountries.ui.model.Resource
import com.mandomi.worldcountries.ui.model.ResourceState
import javax.inject.Inject

class CountriesViewModel @Inject constructor(private val useCase: GetCountriesUseCase) : BaseViewModel() {

    private val data: MutableLiveData<Resource<List<CountryItem>>> = MutableLiveData()

    init {
        useCases += useCase
    }

    fun load(query: String = "") {
        data.postValue(Resource(ResourceState.LOADING))
        useCase.execute(GetCountriesUseCase.Params(query), ::success, ::error)
    }

    fun getData(): LiveData<Resource<List<CountryItem>>> {
        return data
    }

    private fun error(throwable: Throwable) {
        data.postValue(Resource(ResourceState.ERROR, failure = throwable))
    }

    private fun success(countries: List<Country>) {
        data.postValue(Resource(ResourceState.SUCCESS, countries.map { it.toCountryItem() }))
    }
}