package com.mandomi.worldcountries.ui.country.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar
import com.mandomi.worldcountries.R
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.extension.createViewModel
import com.mandomi.worldcountries.extension.observe
import com.mandomi.worldcountries.ui.base.BaseFragment
import com.mandomi.worldcountries.ui.model.Resource
import com.mandomi.worldcountries.ui.model.ResourceState.*
import kotlinx.android.synthetic.main.fragment_country_detail.*
import java.text.NumberFormat
import java.util.*

class CountryDetailFragment : BaseFragment() {
    private lateinit var viewModel: CountryDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_country_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = createViewModel(viewModelFactory) {
            observe(getData(), ::handleResourceState)
        }
        viewModel.load(CountryDetailFragmentArgs.fromBundle(arguments!!).countryName)
    }

    private fun handleResourceState(resource: Resource<CountryDetail>?) {
        resource?.let {
            when (resource.resourceState) {
                LOADING -> handleLoading()
                SUCCESS -> handleSuccess(resource.data!!)
                ERROR -> handleError(resource.failure!!)
            }
        }
    }

    private fun handleLoading() {
        hideDetailView()
        hideEmptyView()
        hideErrorView()
        showLoading()
    }

    private fun hideDetailView() {
        detailGroup.visibility = View.GONE
    }

    private fun hideEmptyView() {
        emptyView.visibility = View.GONE
    }

    private fun hideErrorView() {
        errorView.visibility = View.GONE
    }

    private fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    private fun handleSuccess(data: CountryDetail?) {
        hideErrorView()
        hideLoading()
        if (data == null) {
            hideDetailView()
            showEmptyView()
        } else {
            hideEmptyView()
            showDetailView()
            showData(data)
        }
    }

    private fun hideLoading() {
        loading.visibility = View.GONE
    }

    private fun showEmptyView() {
        emptyView.visibility = View.VISIBLE
    }

    private fun showDetailView() {
        detailGroup.visibility = View.VISIBLE
    }

    private fun showData(data: CountryDetail) {
        image.setImageResource(
            requireContext().resources.getIdentifier(
                "ic_${data.alpha2Code.toLowerCase()}",
                "drawable",
                requireContext().packageName
            )
        )

        if (data.nativeName.isNullOrEmpty()) {
            name.text = data.name
        } else {
            name.text = String.format(resources.getString(R.string.name_and_native_name), data.name, data.nativeName)
        }


        capitalValue.text = data.capitalCity
        languagesValue.text = data.languages?.map { it?.name }?.joinToString()
        populationValue.text = NumberFormat.getNumberInstance(Locale.US).format(data.population!!)
        areaValue.text = NumberFormat.getNumberInstance(Locale.US).format(data.area!!)
        currenciesValue.text = data.currencies?.joinToString {
            String.format(resources.getString(R.string.currency_and_symbol), it?.name, it?.symbol)
        }
        timeZonesValue.text = data.timeZones?.joinToString()
        callingCodesValue.text = data.callingCodes.joinToString { "+$it" }

        if (!data.region.isNullOrEmpty() && !data.subRegion.isNullOrEmpty()) {
            regionValue.text =
                    String.format(resources.getString(R.string.region_and_sub_region), data.region, data.subRegion)
        } else if (!data.region.isNullOrEmpty()) {
            regionValue.text = data.region
        } else if (!data.subRegion.isNullOrEmpty()) {
            regionValue.text = data.subRegion
        }

    }

    private fun handleError(failure: Throwable) {
        hideLoading()
        hideDetailView()
        hideEmptyView()
        showErrorView()
        showSnackBar(failure.message)
    }

    private fun showErrorView() {
        errorView.visibility = View.VISIBLE
    }

    private fun showSnackBar(message: String?) {
        message?.let {
            Snackbar.make(view!!, message, Snackbar.LENGTH_INDEFINITE).apply {
                view.layoutParams = (view.layoutParams as FrameLayout.LayoutParams).apply {
                    setAction(getString(R.string.try_again)) {
                        viewModel.load(
                            CountryDetailFragmentArgs.fromBundle(
                                arguments!!
                            ).countryName
                        )
                    }
                }
            }.show()
        }
    }
}