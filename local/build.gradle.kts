import com.android.build.gradle.internal.dsl.TestOptions
import com.mandomi.worldcountries.gradle.Config
import com.mandomi.worldcountries.gradle.TestLibs.androidXTestRunner
import com.mandomi.worldcountries.gradle.TestLibs.mockk
import com.mandomi.worldcountries.gradle.TestLibs.junit
import com.mandomi.worldcountries.gradle.TestLibs.robolectric
import com.mandomi.worldcountries.gradle.TestLibs.roomTesting
import com.mandomi.worldcountries.gradle.Libs.roomComoiler
import com.mandomi.worldcountries.gradle.Libs.roomRuntime
import com.mandomi.worldcountries.gradle.implementations
import com.mandomi.worldcountries.gradle.kapts
import com.mandomi.worldcountries.gradle.testImplementations

plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(Config.compileSdkVersion)

    defaultConfig {
        minSdkVersion(Config.minSdkVersion)
        targetSdkVersion(Config.targetSdkVersion)
        versionCode = Config.versionCode
        versionName = Config.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        javaCompileOptions {
            annotationProcessorOptions {
                arguments = mapOf("room.schemaLocation" to "$projectDir/schemas")
            }
        }
    }

    sourceSets {
        getByName("androidTest").java.srcDirs("$projectDir/schemas")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles("proguard-rules.pro", getDefaultProguardFile("proguard-android.txt"))
        }
    }

    testOptions {
        unitTests(delegateClosureOf<TestOptions.UnitTestOptions> {
            isIncludeAndroidResources = true
        })
    }
}

dependencies {
    implementations {
        +project(":data")
        +roomRuntime
    }
    kapts {
        +roomComoiler
    }
    testImplementations {
        +robolectric
        +mockk
        +junit
        +roomTesting
        +androidXTestRunner
    }
}