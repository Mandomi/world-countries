package com.mandomi.worldcountries.local.boundry

import com.mandomi.worldcountries.local.entity.CountryEntity

fun CountryEntity.toDataCountryEntity(): com.mandomi.worldcountries.data.entity.CountryEntity {
    return com.mandomi.worldcountries.data.entity.CountryEntity(
        name,
        nativeName,
        capitalCity,
        alpha2Code,
        listOf(callingCode),
        region
    )
}

fun com.mandomi.worldcountries.data.entity.CountryEntity.toLocalCountryEntity(): CountryEntity {
    return CountryEntity(
        null,
        name,
        nativeName,
        capitalCity,
        alpha2Code,
        callingCodes[0],
        region
    )
}