package com.mandomi.worldcountries.local.datasource

import com.mandomi.worldcountries.data.country.CountryLocalDataSource
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.local.boundry.toDataCountryEntity
import com.mandomi.worldcountries.local.boundry.toLocalCountryEntity
import com.mandomi.worldcountries.local.dao.CountryDao
import javax.inject.Inject

class CountryLocalDataSourceImpl @Inject constructor(private val countryDao: CountryDao) : CountryLocalDataSource {
    override suspend fun countries(): List<CountryEntity> {
        return countryDao.getAll().map { it.toDataCountryEntity() }
    }

    override suspend fun add(countryEntity: CountryEntity) {
        countryDao.insert(countryEntity.toLocalCountryEntity())
    }

    override suspend fun add(countries: List<CountryEntity>) {
        countryDao.insertAll(countries.map { it.toLocalCountryEntity() })
    }

    override suspend fun deleteAll() {
        countryDao.deleteAll()
    }

}