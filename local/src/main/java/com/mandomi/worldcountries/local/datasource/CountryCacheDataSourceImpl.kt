package com.mandomi.worldcountries.local.datasource

import com.mandomi.worldcountries.data.country.CountryCacheDataSource
import com.mandomi.worldcountries.data.entity.CountryEntity
import javax.inject.Inject

class CountryCacheDataSourceImpl @Inject constructor() : CountryCacheDataSource {

    private val cachedCountries = mutableListOf<CountryEntity>()

    override fun countries(): List<CountryEntity> {
        return cachedCountries
    }

    override fun add(countryEntity: CountryEntity) {
        cachedCountries.add(countryEntity)
    }

    override fun add(countries: List<CountryEntity>) {
        cachedCountries.addAll(countries)
    }

    override fun deleteAll() {
        cachedCountries.clear()
    }

    override fun isCached(): Boolean {
        return cachedCountries.isNotEmpty()
    }

}