package com.mandomi.worldcountries.local.dao

import androidx.room.*
import com.mandomi.worldcountries.local.entity.CountryEntity

@Dao
interface CountryDao {
    @Query("SELECT * FROM country")
    fun getAll(): List<CountryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(countryEntity: CountryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(countryEntity: List<CountryEntity>)

    @Update
    fun update(countryEntity: CountryEntity)

    @Delete
    fun delete(countryEntity: CountryEntity)

    @Query("DELETE FROM country")
    fun deleteAll()
}