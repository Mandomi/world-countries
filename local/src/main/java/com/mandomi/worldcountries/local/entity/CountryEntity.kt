package com.mandomi.worldcountries.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "country")
data class CountryEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val alpha2Code: String,
    val callingCode: String,
    val region: String?
)