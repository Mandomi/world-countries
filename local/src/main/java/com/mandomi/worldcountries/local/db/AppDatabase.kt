package com.mandomi.worldcountries.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mandomi.worldcountries.local.dao.CountryDao
import com.mandomi.worldcountries.local.entity.CountryEntity

@Database(
    version = 1,
    entities = [
        CountryEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun countryDao(): CountryDao
}