package com.mandomi.worldcountries.local.factory

import com.mandomi.worldcountries.local.factory.DataFactory.randomString
import com.mandomi.worldcountries.local.entity.CountryEntity

internal object CountryFactory {
    fun makeLocalCountryEntityList(count: Int): List<CountryEntity> {
        val countries = mutableListOf<CountryEntity>()
        repeat(count) {
            countries.add(makeLocalCountry())
        }
        return countries
    }

    fun makeDataCountryEntityList(count: Int): List<com.mandomi.worldcountries.data.entity.CountryEntity> {
        val countries = mutableListOf<com.mandomi.worldcountries.data.entity.CountryEntity>()
        repeat(count) {
            countries.add(makeDataCountry())
        }
        return countries
    }

    private fun makeLocalCountry(): CountryEntity {
        return CountryEntity(
            null,
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            randomString()
        )
    }

    fun makeDataCountry(): com.mandomi.worldcountries.data.entity.CountryEntity {
        return com.mandomi.worldcountries.data.entity.CountryEntity(
            randomString(),
            randomString(),
            randomString(),
            randomString(),
            listOf(randomString()),
            randomString()
        )
    }
}