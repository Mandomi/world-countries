package com.mandomi.worldcountries.local.datasource

import com.mandomi.worldcountries.local.boundry.toLocalCountryEntity
import com.mandomi.worldcountries.local.dao.CountryDao
import com.mandomi.worldcountries.local.factory.CountryFactory.makeDataCountry
import com.mandomi.worldcountries.local.factory.CountryFactory.makeDataCountryEntityList
import com.mandomi.worldcountries.local.factory.CountryFactory.makeLocalCountryEntityList
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CountryLocalDataSourceImplTest {

    private val countryDao: CountryDao = mockk(relaxUnitFun = true)
    private val dataSource = CountryLocalDataSourceImpl(countryDao)


    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountries works`() = runBlocking {
        val countries = makeLocalCountryEntityList(10)
        every { countryDao.getAll() } returns countries

        val result = dataSource.countries()

        verify(exactly = 1) { countryDao.getAll() }
        assertEquals(result.map { it.toLocalCountryEntity() }, countries)
    }

    @Test
    fun `add a CountryEntity`() = runBlocking {
        val country = makeDataCountry()

        dataSource.add(country)

        verify(exactly = 1) { countryDao.insert(country.toLocalCountryEntity()) }
    }

    @Test
    fun `add a list of CountryEntity`() = runBlocking {
        val countries = makeDataCountryEntityList(5)

        dataSource.add(countries)

        verify(exactly = 1) { countryDao.insertAll(countries.map { it.toLocalCountryEntity() }) }
    }

    @Test
    fun `delete all works`() = runBlocking {
        dataSource.deleteAll()

        verify(exactly = 1) { countryDao.deleteAll() }
    }
}