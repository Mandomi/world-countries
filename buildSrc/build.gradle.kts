plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_6
    targetCompatibility = JavaVersion.VERSION_1_6
}