package com.mandomi.worldcountries.gradle

internal object Versions {
    // build libraries
    const val androidGradle = "3.2.1"
    const val kotlin = "1.3.11"
    // app libraries
    const val androidX = "1.0.1"
    const val supportLibrary = "1.0.2"
    const val recyclerView = "1.0.0"
    const val constraintLayout = "2.0.0-alpha3"
    const val coroutines = "1.1.0"
    const val javaxAnnotation = "1.3.2"
    const val javaxInject = "1"
    const val retrofit = "2.4.0"
    const val okHttp = "3.11.0"
    const val gson = "2.8.5"
    const val room = "2.0.0"
    const val navigation = "1.0.0-alpha09"
    const val architectureComponents = "2.0.0-rc01"
    const val dagger = "2.16"
    // test libraries
    const val junit = "4.12"
    const val androidTest = "1.1.0"
    const val espresso = "3.1.0"
    const val mockk = "1.8.13.kotlin13"
    const val robolectric = "4.0-alpha-3"
    const val supportCoreTesting = "1.1.1"
}

object Libs {
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val androidxCore = "androidx.core:core-ktx:${Versions.androidX}"
    const val appcompat = "androidx.appcompat:appcompat:${Versions.supportLibrary}"
    const val constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val coroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val javaxAnnotation = "javax.annotation:javax.annotation-api:${Versions.javaxAnnotation}"
    const val javaxInject = "javax.inject:javax.inject:${Versions.javaxInject}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"
    const val okhttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val gson = "com.google.code.gson:gson:${Versions.gson}"
    const val roomRuntime = "androidx.room:room-runtime:${Versions.room}"
    const val roomComoiler = "androidx.room:room-compiler:${Versions.room}"
    const val navigationFragment = "android.arch.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUI = "android.arch.navigation:navigation-ui-ktx:${Versions.navigation}"
    const val recyclerview = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.architectureComponents}"
    const val dagger = "com.google.dagger:dagger:${Versions.dagger}"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val daggerAndroid = "com.google.dagger:dagger-android:${Versions.dagger}"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:${Versions.dagger}"
    const val daggerAndroidProcessor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
}

object TestLibs {
    const val junit = "junit:junit:${Versions.junit}"
    const val androidXTestRunner = "androidx.test:runner:${Versions.androidTest}"
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val mockk = "io.mockk:mockk:${Versions.mockk}"
    const val roomTesting = "androidx.room:room-testing:${Versions.room}"
    const val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"
    const val androidXCoreTesting = "android.arch.core:core-testing:${Versions.supportCoreTesting}"
    const val androidXTestRules = "androidx.test:rules:${Versions.androidTest}"
}

object GradlePlugins {
    const val android = "com.android.tools.build:gradle:${Versions.androidGradle}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val androidNavigation = "android.arch.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
}