package com.mandomi.worldcountries.gradle

object Config {
    const val compileSdkVersion = 28
    const val targetSdkVersion = 28
    const val minSdkVersion = 17
    const val applicationId = "com.mandomi.worldcountries"
    const val versionCode = 1
    const val versionName = "1.0.0"
}