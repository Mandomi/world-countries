package com.mandomi.worldcountries.gradle

import org.gradle.kotlin.dsl.DependencyHandlerScope

class DependencyList {
    private val dependencies = mutableListOf<Any>()
    operator fun Any.unaryPlus() {
        dependencies.add(this)
    }

    fun build() = dependencies
}

inline fun DependencyHandlerScope.implementations(init: DependencyList.() -> Unit) {
    addDependencyList("implementation", init)
}

inline fun DependencyHandlerScope.annotationProcessors(init: DependencyList.() -> Unit) {
    addDependencyList("annotationProcessor", init)
}

inline fun DependencyHandlerScope.apis(init: DependencyList.() -> Unit) {
    addDependencyList("api", init)
}

inline fun DependencyHandlerScope.kapts(init: DependencyList.() -> Unit) {
    addDependencyList("kapt", init)
}

inline fun DependencyHandlerScope.testImplementations(init: DependencyList.() -> Unit) {
    addDependencyList("testImplementation", init)
}

inline fun DependencyHandlerScope.androidTestImplementations(init: DependencyList.() -> Unit) {
    addDependencyList("androidTestImplementation", init)
}

inline fun DependencyHandlerScope.addDependencyList(
    configurationName: String,
    init: DependencyList.() -> Unit
) {
    val dependencyList = DependencyList()
    dependencyList.init()
    dependencyList.build().apply {
        forEach { dependencyNotation ->
            add(configurationName, dependencyNotation)
        }
    }
}


