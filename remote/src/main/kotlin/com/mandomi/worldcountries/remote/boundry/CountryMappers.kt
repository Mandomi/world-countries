package com.mandomi.worldcountries.remote.boundry

import com.mandomi.worldcountries.data.entity.CountryDetailEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.domain.entity.Location
import com.mandomi.worldcountries.remote.dto.CountryDetailDto
import com.mandomi.worldcountries.remote.dto.CountryDto

fun CountryDto.toCountryEntity() = CountryEntity(name, nativeName, capitalCity, alpha2Code, callingCodes, region)

fun CountryDetailDto.toCountryDetailEntity(): CountryDetailEntity {
    return CountryDetailEntity(
        name,
        nativeName,
        capitalCity,
        region,
        subRegion,
        Location(location?.get(0), location?.get(1)),
        borders,
        population,
        area,
        languages,
        currencies,
        domains,
        timeZones,
        alpha2Code,
        callingCodes
    )
}