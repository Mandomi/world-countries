package com.mandomi.worldcountries.remote.dto

import com.google.gson.annotations.SerializedName

data class ErrorResponseDto(@SerializedName("message") val message: String)