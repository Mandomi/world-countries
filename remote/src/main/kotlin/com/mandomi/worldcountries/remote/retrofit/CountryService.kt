package com.mandomi.worldcountries.remote.retrofit

import com.mandomi.worldcountries.remote.dto.CountryDetailDto
import com.mandomi.worldcountries.remote.dto.CountryDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryService {

    @GET("all?fields=name;nativeName;capital;alpha2Code;callingCodes;region")
    fun getAllCountries(): Call<List<CountryDto>>

    @GET("name/{name}")
    fun getCountryDetailByName(@Path("name") name: String): Call<List<CountryDetailDto>>

    @GET("alpha/{code}")
    fun getCountryDetailByCode(@Path("code") code: String): Call<List<CountryDetailDto>>
}