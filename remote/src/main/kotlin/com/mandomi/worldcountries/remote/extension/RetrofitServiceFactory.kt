package com.mandomi.worldcountries.remote.extension

import com.mandomi.worldcountries.remote.RemoteConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

inline fun <reified S> createRetrofitService(
    okHttpClient: OkHttpClient?
): S {
    val retrofitBuilder = Retrofit.Builder().baseUrl(RemoteConstants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
    okHttpClient?.let { retrofitBuilder.client(it) }
    return retrofitBuilder.build().create(S::class.java)
}