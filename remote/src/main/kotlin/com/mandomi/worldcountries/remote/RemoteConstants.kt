package com.mandomi.worldcountries.remote

class RemoteConstants {
    companion object {
        const val BASE_URL = "https://restcountries.eu/rest/v2/"
    }
}