package com.mandomi.worldcountries.remote.dto

import com.google.gson.annotations.SerializedName

data class CountryDto(
    @SerializedName("name") val name: String,
    @SerializedName("nativeName") val nativeName: String?,
    @SerializedName("capital") val capitalCity: String?,
    @SerializedName("alpha2Code") val alpha2Code: String,
    @SerializedName("callingCodes") val callingCodes: List<String>,
    @SerializedName("region") val region: String?
)