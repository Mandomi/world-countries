package com.mandomi.worldcountries.remote.extension

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.Error
import com.mandomi.worldcountries.domain.model.Error.*
import com.mandomi.worldcountries.remote.dto.ErrorResponseDto
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import kotlin.coroutines.resume


suspend fun <T, R> Call<T>.awaitEither(map: (T) -> R): Either<R> = suspendCancellableCoroutine { continuation ->
    try {
        enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                continuation.resume(Either.Failure(asNetworkException(t)))
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    try {
                        continuation.resume(Either.Success(map(response.body()!!)))
                    } catch (throwable: Throwable) {
                        continuation.resume(Either.Failure(asNetworkException(throwable)))
                    }
                } else {
                    val t = retrofit2.HttpException(response)
                    continuation.resume(Either.Failure(asNetworkException(t)))
                }
            }
        })
    } catch (t: Throwable) {
        continuation.resume(Either.Failure(asNetworkException(t)))
    }
    continuation.invokeOnCancellation {
        cancel()
    }
}

private fun createHttpError(throwable: HttpException): Error {
    val response = throwable.response()
    val serverErrorMessage = "Server Error"
    return if (response?.errorBody() == null) {
        ServerError(serverErrorMessage)
    } else {
        try {
            val fromJson = Gson().fromJson(response.errorBody()!!.string(), ErrorResponseDto::class.java)
            HttpError(fromJson.message)
        } catch (e: JsonSyntaxException) {
            ServerError(serverErrorMessage)
        } catch (e: IOException) {
            ServerError(serverErrorMessage)
        } catch (e: IllegalStateException) {
            ServerError(serverErrorMessage)
        } catch (e: ClassCastException) {
            ServerError(serverErrorMessage)
        }
    }
}

private fun asNetworkException(throwable: Throwable): Error {
    return when (throwable) {
        is IOException -> NetworkConnectionError("No Network Connection")
        is HttpException -> createHttpError(throwable)
        is Error -> throwable
        else -> ServerError("Server Error")
    }
}
