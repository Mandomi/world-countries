package com.mandomi.worldcountries.remote.datasource

import com.mandomi.worldcountries.data.country.CountryRemoteDataSource
import com.mandomi.worldcountries.data.entity.CountryDetailEntity
import com.mandomi.worldcountries.data.entity.CountryEntity
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.remote.boundry.toCountryDetailEntity
import com.mandomi.worldcountries.remote.boundry.toCountryEntity
import com.mandomi.worldcountries.remote.extension.awaitEither
import com.mandomi.worldcountries.remote.retrofit.CountryService
import javax.inject.Inject

class CountryRemoteDataSourceImpl @Inject constructor(private val service: CountryService) : CountryRemoteDataSource {
    override suspend fun countries(): Either<List<CountryEntity>> {
        return service.getAllCountries().awaitEither { countries -> countries.map { it.toCountryEntity() } }
    }

    override suspend fun countryDetailByName(name: String): Either<CountryDetailEntity> {
        return service.getCountryDetailByName(name).awaitEither { it[0].toCountryDetailEntity() }
    }

    override suspend fun countryDetailByCode(code: String): Either<CountryDetailEntity> {
        return service.getCountryDetailByCode(code).awaitEither { it[0].toCountryDetailEntity() }
    }

}