package com.mandomi.worldcountries.remote.dto

import com.google.gson.annotations.SerializedName
import com.mandomi.worldcountries.domain.entity.Currency
import com.mandomi.worldcountries.domain.entity.Language

data class CountryDetailDto(
    @SerializedName("name") val name: String,
    @SerializedName("nativeName") val nativeName: String?,
    @SerializedName("capital") val capitalCity: String?,
    @SerializedName("region") val region: String?,
    @SerializedName("subregion") val subRegion: String?,
    @SerializedName("latlng") val location: List<Double?>?,
    @SerializedName("borders") val borders: List<String?>?,
    @SerializedName("population") val population: Long?,
    @SerializedName("area") val area: Double?,
    @SerializedName("languages") val languages: List<Language?>?,
    @SerializedName("currencies") val currencies: List<Currency?>?,
    @SerializedName("topLevelDomain") val domains: List<String?>?,
    @SerializedName("timezones") val timeZones: List<String?>?,
    @SerializedName("alpha2Code") val alpha2Code: String,
    @SerializedName("callingCodes") val callingCodes: List<String>
)