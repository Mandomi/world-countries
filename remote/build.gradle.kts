import com.mandomi.worldcountries.gradle.Libs.gson
import com.mandomi.worldcountries.gradle.TestLibs.mockk
import com.mandomi.worldcountries.gradle.TestLibs.junit
import com.mandomi.worldcountries.gradle.Libs.okhttp
import com.mandomi.worldcountries.gradle.Libs.okhttpLoggingInterceptor
import com.mandomi.worldcountries.gradle.Libs.retrofit
import com.mandomi.worldcountries.gradle.Libs.retrofitGsonConverter
import com.mandomi.worldcountries.gradle.apis
import com.mandomi.worldcountries.gradle.implementations
import com.mandomi.worldcountries.gradle.testImplementations

plugins {
    kotlin("jvm")
    `java-library`
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    apis {
        +okhttp
        +okhttpLoggingInterceptor
        +retrofitGsonConverter
        +gson
    }
    implementations {
        +project(":data")
        +retrofit
    }
    testImplementations {
        +mockk
        +junit
    }
}