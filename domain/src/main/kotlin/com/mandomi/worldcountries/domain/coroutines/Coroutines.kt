package com.mandomi.worldcountries.domain.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext

/**
 * Uses [withContext] instead of [async].
 *
 * @see [async vs withcontext](https://stackoverflow.com/questions/50230466/kotlin-withcontext-vs-async-await][withcontex vs async)
 */
suspend fun <T> awaitIO(block: suspend CoroutineScope.() -> T) =
    withContext(context = GlobalDispatchers.IO, block = block)