package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.interactor.base.CoroutineUseCase
import com.mandomi.worldcountries.domain.repository.CountryRepository
import javax.inject.Inject

class GetCountryDetailByName @Inject constructor(private val countryRepository: CountryRepository) :
    CoroutineUseCase<CountryDetail, GetCountryDetailByName.Params>() {
    override suspend fun executeOnBackground(params: Params): Either<CountryDetail> {
        return countryRepository.countryDetailByName(params.name)
    }

    data class Params(val name: String)
}