package com.mandomi.worldcountries.domain.repository

import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.model.Either

interface CountryRepository {

    suspend fun countries(): Either<List<Country>>
    suspend fun countryDetailByName(name: String): Either<CountryDetail>
    suspend fun countryDetailByCode(code: String): Either<CountryDetail>

}