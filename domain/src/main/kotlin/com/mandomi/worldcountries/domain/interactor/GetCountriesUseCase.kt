package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.interactor.base.CoroutineUseCase
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.repository.CountryRepository
import javax.inject.Inject

class GetCountriesUseCase @Inject constructor(private val countryRepository: CountryRepository) :
    CoroutineUseCase<List<Country>, GetCountriesUseCase.Params>() {

    override suspend fun executeOnBackground(params: Params): Either<List<Country>> {
        val result = countryRepository.countries()
        if (result is Either.Success) {
            val query = params.query.toLowerCase()

            if (query.isNotEmpty()) {
                val filteredData = mutableListOf<Pair<Country, Int>>()
                result.value.forEach { country ->
                    val weight = filter(query, country)
                    if (weight != 0) {
                        filteredData.add(Pair(country, weight))
                    }
                }

                filteredData.sortByDescending { it.second }
                val returnData = mutableListOf<Country>()
                filteredData.forEach { returnData.add(it.first) }
                return Either.Success(returnData)
            }
        }
        return result
    }

    private fun filter(query: String, country: Country): Int {

        var tempQuery = query
        if (query.startsWith("+")){
            tempQuery = query.substring(1)
        }

        with(country.name.toLowerCase()) {
            if (equals(tempQuery)) return Int.MAX_VALUE
            else if (contains(tempQuery)) return 6
        }

        if (!country.nativeName.isNullOrEmpty()) {
            with(country.nativeName.toLowerCase()) {
                if (equals(tempQuery)) return Int.MAX_VALUE
                else if (contains(tempQuery)) return 5
            }
        }
        if (!country.capitalCity.isNullOrEmpty()) {
            with(country.capitalCity.toLowerCase()) {
                if (equals(tempQuery)) return Int.MAX_VALUE
                else if (contains(tempQuery)) return 4
            }
        }
        if (country.alpha2Code.isNotEmpty()) {
            with(country.alpha2Code.toLowerCase()) {
                if (equals(tempQuery)) return Int.MAX_VALUE
                else if (contains(tempQuery)) return 3
            }
        }
        if (!country.region.isNullOrEmpty()) {
            with(country.region.toLowerCase()) {
                if (equals(tempQuery)) return Int.MAX_VALUE
                else if (contains(tempQuery)) return 2
            }
        }
        if (country.callingCodes[0].isNotEmpty()) {
            with(country.callingCodes[0].toLowerCase()) {
                if (equals(tempQuery)) return Int.MAX_VALUE
                else if (contains(tempQuery)) return 1
            }
        }

        return 0
    }

    data class Params(val query: String= "")
}