package com.mandomi.worldcountries.domain.model

sealed class Error(override val message: String) : Throwable(message) {
    class NetworkConnectionError(message: String) : Error(message)
    class ServerError(message: String) : Error(message)
    class HttpError(message: String) : Error(message)

    /** * Extend this class for feature specific failures.*/
    abstract class FeatureError(message: String) : Error(message)
}