package com.mandomi.worldcountries.domain.entity

data class Country(
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val alpha2Code: String,
    val callingCodes: List<String>,
    val region: String?
)