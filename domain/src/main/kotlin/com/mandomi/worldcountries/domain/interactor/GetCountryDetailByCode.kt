package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.interactor.base.CoroutineUseCase
import com.mandomi.worldcountries.domain.repository.CountryRepository
import javax.inject.Inject

class GetCountryDetailByCode @Inject constructor(private val countryRepository: CountryRepository) :
    CoroutineUseCase<CountryDetail, GetCountryDetailByCode.Params>() {
    override suspend fun executeOnBackground(params: Params): Either<CountryDetail> {
        return countryRepository.countryDetailByCode(params.code)
    }

    data class Params(val code: String)
}