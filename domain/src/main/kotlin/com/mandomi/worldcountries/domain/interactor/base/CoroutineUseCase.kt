package com.mandomi.worldcountries.domain.interactor.base

import com.mandomi.worldcountries.domain.coroutines.GlobalDispatchers
import com.mandomi.worldcountries.domain.coroutines.awaitIO
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.fold
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch

abstract class CoroutineUseCase<Result, in Params> : UseCase {

    private val job: Job = Job()

    fun execute(
        params: Params,
        success: (result: Result) -> Unit,
        error: (e: Throwable) -> Unit
    ) {
        CoroutineScope(GlobalDispatchers.Main + job).launch {
            awaitIO { executeOnBackground(params) }.fold(success, error)
        }
    }

    protected abstract suspend fun executeOnBackground(params: Params): Either<Result>

    override fun dispose() {
        with(job) {
            cancelChildren()
            cancel()
        }
    }
}