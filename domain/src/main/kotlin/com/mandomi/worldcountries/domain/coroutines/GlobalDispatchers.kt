package com.mandomi.worldcountries.domain.coroutines

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
 * Holds all needed CoroutineContext]s.
 *
 * Same as [Dispatchers] but allows object mocking for testing purposes.
 */
object GlobalDispatchers {
    val Main: CoroutineContext = Dispatchers.Main
    val IO: CoroutineContext = Dispatchers.IO
}
