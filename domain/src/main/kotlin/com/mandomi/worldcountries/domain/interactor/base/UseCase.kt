package com.mandomi.worldcountries.domain.interactor.base

interface UseCase {
    fun dispose()
}