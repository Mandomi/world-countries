package com.mandomi.worldcountries.domain.entity

data class CountryDetail(
    val name: String,
    val nativeName: String?,
    val capitalCity: String?,
    val region: String?,
    val subRegion: String?,
    val location: Location?,
    val borders: List<String?>?,
    val population: Long?,
    val area: Double?,
    val languages: List<Language?>?,
    val currencies: List<Currency?>?,
    val domains: List<String?>?,
    val timeZones: List<String?>?,
    val alpha2Code: String,
    val callingCodes: List<String>
)

data class Location(val lat: Double?, val lang: Double?)

data class Currency(val name: String, val code: String, val symbol: String)

data class Language(val name: String, val nativeName: String, val iso639_2: String)