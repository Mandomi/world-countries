package com.mandomi.worldcountries.domain.model

import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class EitherTest {

    private val error: Error = mockk()
    private var result: Either<String>? = null

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getOrNull returns actual value when result is success`() {
        result = Either.Success("value")
        assertEquals("value", result!!.getOrNull())
    }

    @Test
    fun `getOrNull returns null when result is failure`() {
        result = Either.Failure(error)
        assertNull(result?.getOrNull())
    }

    @Test
    fun `getOrDefault returns actual value when result is success`() {
        result = Either.Success("value")
        assertEquals("value", result?.getOrDefault("second value"))
    }

    @Test
    fun `getOrDefault returns default value when result is failure`() {
        result = Either.Failure(error)
        assertEquals("second value", result?.getOrDefault("second value"))
    }

    @Test
    fun `map transforms success value to a new Either instance`() {
        result = Either.Success("value")
        assertEquals(1, result?.map { 1 }?.getOrNull())
    }

    @Test
    fun `map will not transform failure result`() {
        result = Either.Failure(error)
        every { error.message } returns "mock error"
        assertNull(result!!.map { 1 }.getOrNull())
        assertThat(result, instanceOf(Either.Failure::class.java))
        result?.let {
            when (it) {
                is Either.Failure -> {
                    assertEquals(
                        "mock error", it.error.message
                    )
                }
                else -> {
                    throw IllegalStateException()
                }
            }
        }
    }

    @Test
    fun `folding with isSuccess`() {
        result = Either.Success("value")
        val folded = result!!.fold({
            "success"
        }, {
            "error"
        })
        assertEquals("success", folded)
    }

    @Test
    fun `folding with isFailure`() {
        result = Either.Failure(error)
        val folded = result!!.fold({
            "success"
        }, {
            "error"
        })
        assertEquals("error", folded)
    }
}