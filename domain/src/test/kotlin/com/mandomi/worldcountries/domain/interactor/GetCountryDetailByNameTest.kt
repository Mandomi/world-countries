package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.coroutines.GlobalDispatchers
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.factory.CountryFactory
import com.mandomi.worldcountries.domain.factory.CountryFactory.makeCountryDetail
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.Error
import com.mandomi.worldcountries.domain.repository.CountryRepository
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetCountryDetailByNameTest {
    private val repository: CountryRepository = mockk()
    private val useCase = GetCountryDetailByName(repository)
    private val countryName = "United States"

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountryDetailByName method of repository is called`() = runBlocking {

        coEvery { repository.countryDetailByName(any()) } returns Either.Success(makeCountryDetail())
        mockDispatcher()

        useCase.execute(GetCountryDetailByName.Params(countryName), {}, {})

        coVerify(exactly = 1) { repository.countryDetailByName(countryName) }
    }

    @Test
    fun `success`() = runBlocking {
        val success: (countryDetail: CountryDetail) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()
        val countryDetail = CountryFactory.makeCountryDetail()

        every { success.invoke(any()) } just Runs
        coEvery { repository.countryDetailByName(countryName) } returns Either.Success(countryDetail)
        mockDispatcher()

        useCase.execute(GetCountryDetailByName.Params(countryName), success, error)

        coVerify(exactly = 1) { repository.countryDetailByName(countryName) }
        verify(exactly = 1) { success.invoke(countryDetail) }
        verify { error wasNot called }
    }

    @Test
    fun `fail`() = runBlocking {
        val success: (countryDetail: CountryDetail) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()
        val failure = Error.HttpError("404")

        every { error.invoke(any()) } just Runs
        coEvery { repository.countryDetailByName(any()) } returns Either.Failure(failure)
        mockDispatcher()

        useCase.execute(GetCountryDetailByName.Params(countryName), success, error)

        coVerify(exactly = 1) { repository.countryDetailByName(countryName) }
        verify(exactly = 1) { error.invoke(failure) }
        verify { success wasNot called }
    }

    private fun mockDispatcher() {
        mockkObject(GlobalDispatchers)
        every { GlobalDispatchers.Main } returns Dispatchers.Unconfined
    }
}