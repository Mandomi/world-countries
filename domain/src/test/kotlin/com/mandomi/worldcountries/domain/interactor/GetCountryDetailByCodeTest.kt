package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.coroutines.GlobalDispatchers
import com.mandomi.worldcountries.domain.entity.CountryDetail
import com.mandomi.worldcountries.domain.factory.CountryFactory
import com.mandomi.worldcountries.domain.factory.CountryFactory.makeCountryDetail
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.Error
import com.mandomi.worldcountries.domain.repository.CountryRepository
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetCountryDetailByCodeTest {
    private val repository: CountryRepository = mockk()
    private val useCase = GetCountryDetailByCode(repository)
    private val countryCode = "US"

    @Before
    fun setUp() {
        clearAllMocks()
    }

    @Test
    fun `getCountryDetailByCode method of repository is called`() = runBlocking {

        coEvery { repository.countryDetailByCode(any()) } returns Either.Success(makeCountryDetail())
        mockDispatcher()

        useCase.execute(GetCountryDetailByCode.Params(countryCode), {}, {})

        coVerify(exactly = 1) { repository.countryDetailByCode(countryCode) }
    }

    @Test
    fun `success`() = runBlocking {
        val success: (countryDetail: CountryDetail) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()
        val countryDetail = CountryFactory.makeCountryDetail()

        every { success.invoke(any()) } just Runs
        coEvery { repository.countryDetailByCode(countryCode) } returns Either.Success(countryDetail)
        mockDispatcher()

        useCase.execute(GetCountryDetailByCode.Params(countryCode), success, error)

        coVerify(exactly = 1) { repository.countryDetailByCode(countryCode) }
        verify(exactly = 1) { success.invoke(countryDetail) }
        verify { error wasNot called }
    }

    @Test
    fun `fail`() = runBlocking {
        val success: (countryDetail: CountryDetail) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()
        val failure = Error.HttpError("404")

        every { error.invoke(any()) } just Runs
        coEvery { repository.countryDetailByCode(any()) } returns Either.Failure(failure)
        mockDispatcher()

        useCase.execute(GetCountryDetailByCode.Params(countryCode), success, error)

        coVerify(exactly = 1) { repository.countryDetailByCode(countryCode) }
        verify(exactly = 1) { error.invoke(failure) }
        verify { success wasNot called }
    }

    private fun mockDispatcher() {
        mockkObject(GlobalDispatchers)
        every { GlobalDispatchers.Main } returns Dispatchers.Unconfined
    }
}