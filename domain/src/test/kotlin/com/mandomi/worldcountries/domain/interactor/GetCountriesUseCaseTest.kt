package com.mandomi.worldcountries.domain.interactor

import com.mandomi.worldcountries.domain.coroutines.GlobalDispatchers
import com.mandomi.worldcountries.domain.entity.Country
import com.mandomi.worldcountries.domain.factory.CountryFactory.makeCountryList
import com.mandomi.worldcountries.domain.model.Either
import com.mandomi.worldcountries.domain.model.Error
import com.mandomi.worldcountries.domain.repository.CountryRepository
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class GetCountriesUseCaseTest {

    private val repository: CountryRepository = mockk()
    private val useCase = GetCountriesUseCase(repository)
    private val defaultParam = GetCountriesUseCase.Params()

    @Before
    fun setUp() {
        clearAllMocks()
    }


    @Test
    fun `countries method of repository is called`() = runBlocking {
        coEvery { repository.countries() } returns Either.Success(makeCountryList(10))
        mockDispatcher()

        useCase.execute(defaultParam, {}, {})

        coVerify(exactly = 1) { repository.countries() }
    }

    @Test
    fun `success`() = runBlocking {
        val countries = makeCountryList(10)
        val success: (countries: List<Country>) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()

        every { success.invoke(any()) } just Runs
        coEvery { repository.countries() } returns Either.Success(countries)
        mockDispatcher()

        useCase.execute(defaultParam, success, error)

        coVerify(exactly = 1) { repository.countries() }
        verify(exactly = 1) { success.invoke(countries) }
        verify { error wasNot called }
    }

    @Test
    fun `fail`() = runBlocking {
        val success: (countries: List<Country>) -> Unit = mockk()
        val error: (Throwable) -> Unit = mockk()
        val failure = Error.HttpError("404")

        every { error.invoke(any()) } just Runs
        coEvery { repository.countries() } returns Either.Failure(failure)
        mockDispatcher()

        useCase.execute(defaultParam, success, error)

        coVerify(exactly = 1) { repository.countries() }
        verify(exactly = 1) { error.invoke(failure) }
        verify { success wasNot called }
    }

    private fun mockDispatcher() {
        mockkObject(GlobalDispatchers)
        every { GlobalDispatchers.Main } returns Dispatchers.Unconfined
    }
}