import com.mandomi.worldcountries.gradle.Libs.coroutinesAndroid
import com.mandomi.worldcountries.gradle.Libs.coroutinesCore
import com.mandomi.worldcountries.gradle.Libs.javaxAnnotation
import com.mandomi.worldcountries.gradle.Libs.javaxInject
import com.mandomi.worldcountries.gradle.Libs.kotlinStdLib
import com.mandomi.worldcountries.gradle.TestLibs.junit
import com.mandomi.worldcountries.gradle.TestLibs.mockk
import com.mandomi.worldcountries.gradle.apis
import com.mandomi.worldcountries.gradle.implementations
import com.mandomi.worldcountries.gradle.testImplementations

plugins {
    kotlin("jvm")
    `java-library`
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    apis {
        +kotlinStdLib
        +coroutinesCore
        +coroutinesAndroid
        +javaxAnnotation
        +javaxInject
    }
    testImplementations {
        +mockk
        +junit
    }
}
